//
//  Network.swift
//  Pokedex
//
//  Created by Ricardo Ortiz on 20/6/18.
//  Copyright © 2018 Ricardo Ortiz. All rights reserved.
//

import Foundation
import Foundation
import Alamofire

class Network{
    //no es una solucion porque solo espera un solo completion la sol. es DispatchGroup
    //func getAllPokemon(completion:@escaping ([Pokemon])->()){
    func getAllPokemon(completion:@escaping ([Pokemon])->()){
        var pokemonArray:[Pokemon] = []
        let group = DispatchGroup()
        
        for i in 1...8{
            //entra 8 veces
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
                guard let data = response.data else{
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else{
                    print("Error decoding Pokemon")
                    return
                }
                pokemonArray.append(pokemon)
                print(pokemon)
                //sale 8 veces
                group.leave()
            }
        }
        //.main=hilo principal
        group.notify(queue: .main){
            completion(pokemonArray)
        }
    }
    
    func getPokemonImage(url:String) {
        
    }
}
